#   check the validity of k which must be a number bigger than 0 and less or equal to the total number of features

import math
import sys

def checkK(k, inputMatrix):
    if(math.isnan(k)):
        sys.exit('Error! You must input a number')
        #return False
    else:
        if(k > len(inputMatrix.columns)):
            sys.exit('Error! k must be less or equal than the initial amount of feature which is ' + `len(inputMatrix.columns)` + "!")
            #return False
        if(k <= 0):
            sys.exit('Error! k must be greater than 0!')
            #return False