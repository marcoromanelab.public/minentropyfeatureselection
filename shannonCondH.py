#   compute shannon conditional entropy
#   compute H(A|B) = H(B,A) - H(B)

from shannonH import shannonH

def shannonCondH(A, B, res=None):
    if res is not None:
    	res[0] = shannonH(B, A) - shannonH(B)
    return shannonH(B, A) - shannonH(B)
