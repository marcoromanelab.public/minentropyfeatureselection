# __Min-entropy Feature Selection__

## Overview
Program to perform statistical feature selection based on Mutual Information and the greedy 
algorithm described in the paper [Feature selection with Rényi min-entropy](https://hal.archives-ouvertes.fr/hal-01830177v2/document).
Only the feature selection part is made public. The user can use whatever machine learning technique with the selected
features. 
Given that the method relies on the computation of the mutual information between the feature vectors and the 
classification vector, the user has to provide both. In our case we consider that the use passes two pickled pandas dataframes 
whose absolute path is required by the program itself when it is run.  

## How to run the software
It is possible to run the method by issuing the line command

```console
user@server:~$ python main.py
```

The program will list a series of requirements for the user to provide:

```console
Hello! You are using the unifyingFeatureSelection tool!

Absolute path to the features dataframe file --->
```

Now the user must input the absolute path to the dataframe file where all the features are stored (just input the path
as it results from the print working directory command, no quotes are required).

The program now asks to input the path t the classification label vector file:

```console
Absolute path to the labels dataframe file --->
```

Then the path to the folder where all the results are going to be stored is requested:

```console
Absolute path to the results folder --->
```

If the folder does not exist it will be created.

In case the user has already performed the feature selection over a certain dataset and 
just wants to keep selecting features from where the process was interrupted it is possible 
to insert the absolute path of the backup file when requested:

```console
Absolute path to the file containing the set of already selected features --->
```

If the filed is left empty the program will automatically assume that the user wants to 
start from scratch. 

The last requested parameter is the number of features the the user would like to be selected:

```console
Number of features to select --->
```

Of course the user must provide an integer. A function will check if the provided number is
legitimate or not. 

From now on the program will run until the procedure is over. At the end the selected features will be 
printed in the terminal and stored to a file in the result folder. 


---

###### Note for the users
The code runs experiments exploiting the multiprocessing python class and spawning 
8 of them at the same time. We recommend the
usage of a virtual machine in case it is not possible to run such a configuration. 
It might be possible that a new and more flexible version will be released
by the maintainer. The features are numbered from 1 on in the same order as the columns of
the dataframe.

