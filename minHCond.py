#   computing minH(A|B) = -log sum p(B=b) max p(A=a|B=b) ----->
#   -----> -log sum(b) max(a) p(A=a, B=b)

import pandas
import numpy
import math

def minHCond(A, B, res_=None): #A and B must be two pandas.Dataframe
    nColsA = len(A.columns)
    nRowsA = len(A)
    nColsB = len(B.columns)
    nRowsB = len(B)
    minEntropy = 0

    #   creation of join variable A
    arrayJointCombinationsA = []
    for iRows in range(0, nRowsA):
        tmpStr = ""
        for iCols in range(0, nColsA):
            tmpStr += str(A.get_values()[iRows, iCols]) + ","
        arrayJointCombinationsA.append(tmpStr)

    #   creation of join variable A
    arrayJointCombinationsB = []
    for iRows in range(0, nRowsB):
        tmpStr = ""
        for iCols in range(0, nColsB):
            tmpStr += str(B.get_values()[iRows, iCols]) + ","
        arrayJointCombinationsB.append(tmpStr)

    #   concatenation of join variables
    tmpDf = pandas.concat([pandas.DataFrame(arrayJointCombinationsA), pandas.DataFrame(arrayJointCombinationsB)],
                                  axis=1)
    #   computation of join probability keeping distinguished the columns for A and B so it's possible to loop over b
    #   while checking the max looping over a
    tmpDf.columns = ['A', 'B']
    res = tmpDf.groupby(['B', 'A']).size().div(len(tmpDf))
    res = pandas.DataFrame(res).reset_index()
    res.columns = ['B', 'A', 'value']

    #   print arrayJointCombinationsA

    levelsOfB = numpy.unique(res.get_values()[:, 0])
    for el in levelsOfB:
        arr1 = numpy.where(res.get_values()[:, 0] == el)[0]
        maxProb = 0
        for elSel in arr1:
            if(res.get_values()[elSel, 2] > maxProb):
                maxProb = res.get_values()[elSel, 2]
    
        minEntropy += maxProb
    
    minEntropy = -math.log(minEntropy, 2)

    if res_ is not None:
    	res_[0] = minEntropy

    return minEntropy
