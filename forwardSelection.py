#   for Shannon max I(C,fi|S) = min H(C|fi,S)

import pandas
from shannonCondH import shannonCondH
from minHCond import minHCond
import sys
from multiprocessing import Manager, Process


def forwardSelection(featureSet, classLabels, type, selectedFeatureSet=None):
    min = float("inf")
    #   index of fi which gives the min
    indexOfSelectedFeatures = None
    # print min
    #   first features still to be selected selectedFeatureSet has the default value None
    if (selectedFeatureSet is None):
        for idx in range(0, len(featureSet.columns)):
            feat = pandas.DataFrame(featureSet.get_values()[:, idx])
            #   select features using shannon entropy
            if type == "shannon":
                # print "Sannon feature 1"
                tmp = shannonCondH(A=classLabels, B=feat)
            #   select features using renyi minentropy
            elif type == "renyi":
                # print "Renyi feature 1"
                tmp = minHCond(A=classLabels, B=feat)
            else:
                sys.exit("Type of entropy unknown")
            # print tmp
            # print tmp
            if tmp < min:
                min = tmp
                indexOfSelectedFeatures = idx
                # print "min is %f" % min
        # print min
    #   parallelization of feature selection for features after the first
    else:
        #   print min
        remainingFeatures = len(featureSet.columns)
        for idx in range(7, len(featureSet.columns), 8):
            #   select 8 features at each iteration
            f1 = pandas.DataFrame(featureSet.get_values()[:, idx - 7])
            f1.columns = [featureSet.columns.values[idx - 7]]
            f2 = pandas.DataFrame(featureSet.get_values()[:, idx - 6])
            f2.columns = [featureSet.columns.values[idx - 6]]
            f3 = pandas.DataFrame(featureSet.get_values()[:, idx - 5])
            f3.columns = [featureSet.columns.values[idx - 5]]
            f4 = pandas.DataFrame(featureSet.get_values()[:, idx - 4])
            f4.columns = [featureSet.columns.values[idx - 4]]
            f5 = pandas.DataFrame(featureSet.get_values()[:, idx - 3])
            f5.columns = [featureSet.columns.values[idx - 3]]
            f6 = pandas.DataFrame(featureSet.get_values()[:, idx - 2])
            f6.columns = [featureSet.columns.values[idx - 2]]
            f7 = pandas.DataFrame(featureSet.get_values()[:, idx - 1])
            f7.columns = [featureSet.columns.values[idx - 1]]
            f8 = pandas.DataFrame(featureSet.get_values()[:, idx])
            f8.columns = [featureSet.columns.values[idx]]

            listOfIndices = [idx - 7, idx - 6, idx - 5, idx - 4, idx - 3, idx - 2, idx - 1, idx]

            jointFiAndS1 = pandas.concat([f1, selectedFeatureSet], axis=1)
            jointFiAndS2 = pandas.concat([f2, selectedFeatureSet], axis=1)
            jointFiAndS3 = pandas.concat([f3, selectedFeatureSet], axis=1)
            jointFiAndS4 = pandas.concat([f4, selectedFeatureSet], axis=1)
            jointFiAndS5 = pandas.concat([f5, selectedFeatureSet], axis=1)
            jointFiAndS6 = pandas.concat([f6, selectedFeatureSet], axis=1)
            jointFiAndS7 = pandas.concat([f7, selectedFeatureSet], axis=1)
            jointFiAndS8 = pandas.concat([f8, selectedFeatureSet], axis=1)

            #   managing results from paralel processes
            m1 = Manager()
            tmp1 = m1.dict()
            m2 = Manager()
            tmp2 = m2.dict()
            m3 = Manager()
            tmp3 = m3.dict()
            m4 = Manager()
            tmp4 = m4.dict()
            m5 = Manager()
            tmp5 = m5.dict()
            m6 = Manager()
            tmp6 = m6.dict()
            m7 = Manager()
            tmp7 = m7.dict()
            m8 = Manager()
            tmp8 = m8.dict()

            #   select features using shannon entropy
            if type == "shannon":
                # tmp = shannonCondH(A=classLabels, B=jointFiAndS)
                p1 = Process(target=shannonCondH, args=(classLabels, jointFiAndS1, tmp1))
                p2 = Process(target=shannonCondH, args=(classLabels, jointFiAndS2, tmp2))
                p3 = Process(target=shannonCondH, args=(classLabels, jointFiAndS3, tmp3))
                p4 = Process(target=shannonCondH, args=(classLabels, jointFiAndS4, tmp4))
                p5 = Process(target=shannonCondH, args=(classLabels, jointFiAndS5, tmp5))
                p6 = Process(target=shannonCondH, args=(classLabels, jointFiAndS6, tmp6))
                p7 = Process(target=shannonCondH, args=(classLabels, jointFiAndS7, tmp7))
                p8 = Process(target=shannonCondH, args=(classLabels, jointFiAndS8, tmp8))

                p1.start()
                p2.start()
                p3.start()
                p4.start()
                p5.start()
                p6.start()
                p7.start()
                p8.start()

                p1.join()
                p2.join()
                p3.join()
                p4.join()
                p5.join()
                p6.join()
                p7.join()
                p8.join()

            #   select features using renyi minentropy
            elif type == "renyi":  # tmp = minHCond(A=classLabels, B=jointFiAndS)
                # tmp = shannonCondH(A=classLabels, B=jointFiAndS)
                p1 = Process(target=minHCond, args=(classLabels, jointFiAndS1, tmp1))
                p2 = Process(target=minHCond, args=(classLabels, jointFiAndS2, tmp2))
                p3 = Process(target=minHCond, args=(classLabels, jointFiAndS3, tmp3))
                p4 = Process(target=minHCond, args=(classLabels, jointFiAndS4, tmp4))
                p5 = Process(target=minHCond, args=(classLabels, jointFiAndS5, tmp5))
                p6 = Process(target=minHCond, args=(classLabels, jointFiAndS6, tmp6))
                p7 = Process(target=minHCond, args=(classLabels, jointFiAndS7, tmp7))
                p8 = Process(target=minHCond, args=(classLabels, jointFiAndS8, tmp8))

                p1.start()
                p2.start()
                p3.start()
                p4.start()
                p5.start()
                p6.start()
                p7.start()
                p8.start()

                p1.join()
                p2.join()
                p3.join()
                p4.join()
                p5.join()
                p6.join()
                p7.join()
                p8.join()

            else:
                sys.exit("Type of entropy unknown")

            #   selecting among the analyzed features the one with min value for the entropy computation
            res = [tmp1[0], tmp2[0], tmp3[0], tmp4[0], tmp5[0], tmp6[0], tmp7[0], tmp8[0]]
            # print tmp1[0], tmp2[0], tmp3[0], tmp4[0], tmp5[0], tmp6[0], tmp7[0], tmp8[0]
            tmp = float("inf")
            for el in res:
                if el < tmp:
                    tmp = el

            listIdx = res.index(tmp)
            indexOfMin = listOfIndices[listIdx]

            # print tmp
            #   update the value for the overall min of entropy
            if tmp < min:
                min = tmp
                indexOfSelectedFeatures = indexOfMin

            remainingFeatures -= 8

        #   test remaining features excluded from the 8 steps per iteration for cycle
        init = len(featureSet.columns) - remainingFeatures
        # print len(featureSet.columns)
        # print range(init, len(featureSet.columns))
        for j in range(init, len(featureSet.columns)):
            feat = pandas.DataFrame(featureSet.get_values()[:, j])
            feat.columns = [featureSet.columns.values[j]]

            jointFiAndS = pandas.concat([feat, selectedFeatureSet], axis=1)

            if type == "shannon":
                tmp = shannonCondH(A=classLabels, B=jointFiAndS)

            elif type == "renyi":
                tmp = minHCond(A=classLabels, B=jointFiAndS)

            else:
                sys.exit("Type of entropy unknown")

            if tmp < min:
                min = tmp
                indexOfSelectedFeatures = j
        # print "min is %f" % min

    return indexOfSelectedFeatures
