from dataSetup import dataSetup
from selectFeatures import selectFeatures
import os
import time
from retrieveSelFeatFromFile import retrieveSelFeatFromFile
from checkK import *

print ('\nHello! You are using the unifyingFeatureSelection tool!')

########################################################################################################################
###PATHS################################################################################################################
########################################################################################################################

pathToDataFeatures = raw_input("\nAbsolute path to the features dataframe file ---> ")
if not os.path.isfile(pathToDataFeatures):
    sys.exit("Invalid file path")

pathToDataLabels = raw_input("\nAbsolute path to the labels dataframe file ---> ")
if not os.path.isfile(pathToDataLabels):
    sys.exit("Invalid file path")

pathToResultFolder = raw_input("\nAbsolute path to the results folder ---> ")
if not os.path.exists(pathToResultFolder):
    os.makedirs(pathToResultFolder)

selFeatSets_path = raw_input("\nAbsolute path to the file containing the set of already selected features ---> ")
lines = None
if os.path.isfile(selFeatSets_path):
    text_file = open(selFeatSets_path, 'r')
    lines = text_file.readlines()

writePathSelFeatures = pathToResultFolder + '/selectedFeatures'



#   read dataset file
initialData = dataSetup(pathToDataFeatures=pathToDataFeatures, pathToDataLabels=pathToDataLabels)
print ('\nYour dataset has ' + str(initialData[0].shape[0]) + ' samples and ' + str(initialData[0].shape[1]) + ' features.')


initialK = int(raw_input('\nNumber of features to select ---> '))
checkK(k=initialK, inputMatrix=initialData[0])

if lines is None:
    selFeatSets = None
else:
    selFeatSets = retrieveSelFeatFromFile(array=lines)

print("\n")
print("...please wait...")
print("\n")

#   ordering all the features in order of importance for the feature selection criteria
#   select features using Shannon entropy
start_time = time.time()
res = selectFeatures(k=initialK, featureSet_=initialData[0], labelSet_=initialData[2], type="shannon", backup=selFeatSets)
print "\n\nShannon selected features are:"
print res[1].columns.values
elapsed_time = time.time() - start_time
hours, rem = divmod(elapsed_time, 3600)
minutes, seconds = divmod(rem, 60)
print "Selecting " + str(initialK) + " features with Shannon required " + \
      "{:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds)
print "\n"


#   select features using renyi min Entropy
start_time = time.time()
res_ = selectFeatures(k=initialK, featureSet_=initialData[0], labelSet_=initialData[2], type="renyi", backup=selFeatSets)
print("\n\nminEntropy selected features are:")
print res_[1].columns.values
elapsed_time = time.time() - start_time
hours, rem = divmod(elapsed_time, 3600)
minutes, seconds = divmod(rem, 60)
print "Selecting " + str(initialK) + " features with minEntropy required " + \
      "{:0>2}:{:0>2}:{:05.2f}".format(int(hours),int(minutes),seconds)
print("\n\n\n")

#   write selected features to file
if selFeatSets is not None:
    os.remove(writePathSelFeatures)
    f = file(writePathSelFeatures, "wa")
else:
    if os.path.exists(writePathSelFeatures):
        f = file(writePathSelFeatures, "a")
    else:
        f = file(writePathSelFeatures, "wa")
f.write("/////SHANNON/////\n")
f.write("   ".join(res[1].columns.values))
f.write("\n\n\n/////RENYI/////\n")
f.write("   ".join(res_[1].columns.values))
