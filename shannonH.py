#   compute shannon entropy for one variable or joint
#   H(A) = sumi p(ai)*log(p(ai))
#   H(A,B) = sumisumj p(ai,bj)*log(p(ai,bj))

import math
import pandas

def shannonH(A, B = None): #it takes 1 or 2 pandas dataframes

    shannonEntropy = 0

    #   create a unique dataframe for A and B
    if (B is not None):
        A = pandas.concat([A, B], axis=1)

    #if (len(A.columns) == 1):
    #    A = pandas.DataFrame(A.get_values()[:, 0])
    #    A.columns = ['A']
        #freqs = computeFrequences(A)

    # if A has more than one column create a single joint variable
    if(len(A.columns) > 1):  # entropy of a matrix is considered as the joint entropy of the columns
        nCols = len(A.columns)
        nRows = len(A)
        arrayJointCombinations = []
        #   two nested cicles to join all the characters of each row to create a value for the joint variable
        for iRows in range(0, nRows):
            tmpStr = ""
            for iCols in range(0, nCols):
                tmpStr += str(A.get_values()[iRows, iCols]) + ","
            arrayJointCombinations.append(tmpStr)
            #   print arrayJointCombinations
        A = pandas.DataFrame(arrayJointCombinations)

    A.columns = ['A']
    #   compute frequences p(ai)
    freqs = A.groupby('A').size().div(len(A))
    freqs = pandas.DataFrame(freqs).reset_index()
    freqs.columns = ['A', 'value']
    #   compute entropy
    for el in freqs.get_values()[:, freqs.columns.get_loc("value")]:
        shannonEntropy -= el * math.log(el, 2)

    return shannonEntropy












"""import math
import pandas
from computeFrequences import computeFrequences

def shannonH(A, B = None): #it takes 1 or 2 pandas dataframes
    shannonEntropy = 0
    if (B is not None):
        A = pandas.concat([A, B], 1)
    if (len(A.columns) == 1):
        A = A.get_values()[:, 0]
        freqs = computeFrequences(A)

        for idx in range(0, len(freqs)):
            shannonEntropy += freqs[idx] * math.log(freqs[idx], 2)

    else:  # entropy of a matrix is considered as the joint entropy of the columns
        nCols = len(A.columns)
        nRows = len(A)
        arrayJointCombinations = []
        for iRows in range(0, nRows):
            tmpStr = ""
            for iCols in range(0, nCols):
                tmpStr += str(A.get_values()[iRows, iCols])
            arrayJointCombinations.append(tmpStr)
        freqs = computeFrequences(arrayJointCombinations)

        for idx in range(0, len(freqs)):
            shannonEntropy += freqs[idx] * math.log(freqs[idx], 2)

    return -shannonEntropy"""