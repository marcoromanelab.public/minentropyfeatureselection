#   compute renyi entropy with alpha parameter equal to inf, minEntropy
#   H(A) = -log maxi p(A = ai)

import math
import pandas

def minH(A, B=None): #A and B must be two pandas.Dataframe
    max = -float("inf")
    if B is None:
        if (len(A.columns) > 1):  # considering the entropy of a matrix as the entropy of joint of variables which are represented by each column
            print "todo compute joint minEntropy"
            # todo compute joint minEntropy
            """nCols = len(A.columns)
            nRows = len(A)
            arrayJointCombinations = []
            for iRows in range(0, nRows):
                tmpStr = ""
                for iCols in range(0, nCols):
                    tmpStr += str(A.get_values()[iRows, iCols]) + ","
                arrayJointCombinations.append(tmpStr)
            A = pandas.DataFrame(arrayJointCombinations)"""

    else:
        print "todo compute joint minEntropy"
        #todo compute joint minEntropy

    A.columns = ['A']
    freqs = A.groupby('A').size().div(len(A))
    freqs = pandas.DataFrame(freqs).reset_index()
    freqs.columns = ['A', 'value']
    for el in freqs.get_values()[:, freqs.columns.get_loc("value")]:
        if (el > max):
            max = el
    minEntropy = -math.log(max, 2)

    return minEntropy
