from MI import MI
import pandas

def maxMI(featureSet, classLabels):
    maxMI = -float("inf")
    indexOfFeatureWithMaxMI = None

    #   from position 0 to array length - 1
    for idx in range(0, len(featureSet.columns)):

        mi = MI(classLabels, pandas.DataFrame(featureSet.get_values()[:, idx]))

        if (mi > maxMI):
            maxMI = mi
            indexOfFeatureWithMaxMI = idx

    return indexOfFeatureWithMaxMI
