from shannonH import shannonH
from shannonCondH import shannonCondH

def MI(A, B):
    #I(A, B) = H(A) - H(A|B) which is symmetrical
    return (shannonH(A) - shannonCondH(A, B))