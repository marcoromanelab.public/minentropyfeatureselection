#   read files .m matlab datasets through loadmat obtaining a set with the features and an array with labels. Both are
#   organized in the structure of dataframes

import pandas
import numpy
from sklearn.utils import shuffle

def dataSetup(pathToDataFeatures, pathToDataLabels, subSetCard=None):
    #   the structure of these files is such that there's a dataframe for features and dataframe for class labels
    featureSet = pandas.read_pickle(pathToDataFeatures)
    selectedFeatureSet = None
    classLabels = pandas.read_pickle(pathToDataLabels)

    if subSetCard is not None:
        if subSetCard >= featureSet.shape[0]:
            print('Using the whole feature set...')
        else:
            arr = range(0, featureSet.shape[0])
            arr = shuffle(arr)
            arr = arr[0:subSetCard]
            featureSet = pandas.DataFrame(featureSet.get_values()[arr, :])
            classLabels = pandas.DataFrame(classLabels.get_values()[arr, :])

    #   empty array which is going to keep the names of columns
    colnameFeatureSet = numpy.empty([0, 0])

    #   naming every column as f1, f2, ..., fn
    for idx in range(1, len(featureSet.columns)+1):
        colnameFeatureSet = numpy.append(colnameFeatureSet, 'f' + str(idx))

    featureSet.columns = colnameFeatureSet
    classLabels.columns = ["classLabel"]

    #   featureSet is the dataframe with all the features columns and all the samples after naming columns
    #   selectedFeatureSet is the set with selected features and so it's empty now
    #   classLabels is the dataframe with a columns of labels, one for each sample
    res_list = [featureSet, selectedFeatureSet, classLabels]

    return res_list

