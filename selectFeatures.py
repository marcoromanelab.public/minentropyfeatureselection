#   importing data in the correct form after splitting training set and test set from result of
#   setupFeatureSelectionSets

import pandas
import numpy
import copy
from maxMI import maxMI
from forwardSelection import forwardSelection


def selectFeatures(k, featureSet_, labelSet_, type, backup=None):
    #featureSet = copy.deepcopy(inData[0])  #deepcopy needed to prevent delating things from original dataset
    #   python implents argument passing by reference as default
    #classLabels = copy.deepcopy(inData[2])

    featureSet = copy.deepcopy(featureSet_)

    classLabels = copy.deepcopy(labelSet_)


    #   original value of k copied in variable, in this case it's not a structured data so deepcopy is not needed
    kStart = k

    #   importing data in the correct form after splitting training set and test set
    #
    if backup is None:
        #   setting up an empty set for selected features
        selectedFeatureSet = None
        #   setting up an empty array for selected features columns names
        selectedFeatureSetNames = numpy.empty([0, 0])

    else:

        if type == "shannon":
            idxFeat = []
            for el in backup[0]:
                idxFeat.append(featureSet.columns.get_loc(el))
            selectedFeatureSet = pandas.DataFrame(featureSet.get_values()[:, idxFeat])
            selectedFeatureSetNames = numpy.asarray(backup[0])
            selectedFeatureSet.columns = selectedFeatureSetNames
            for el in selectedFeatureSetNames:
                del featureSet[el]
            k = k - len(backup[0])
        else:
            idxFeat = []
            for el in backup[1]:
                idxFeat.append(featureSet.columns.get_loc(el))
            selectedFeatureSet = pandas.DataFrame(featureSet.get_values()[:, idxFeat])
            selectedFeatureSetNames = numpy.asarray(backup[1])
            selectedFeatureSet.columns = selectedFeatureSetNames
            for el in selectedFeatureSetNames:
                del featureSet[el]
            k = k - len(backup[1])

    while k > 0:
        #   this is the first feature the algorithm selects only MI is computed
        if selectedFeatureSet is None:
            #   create the set for selected features
            selectedFeatureSet = pandas.DataFrame()
            #   item = maxMI(featureSet, classLabels)
            #   select first feature, it's the index of the selected column
            item = forwardSelection(featureSet, classLabels, type)
            if item is not None:
                #   add selected feature column to selectedFeatureSet previously empty
                frames = [selectedFeatureSet, pandas.DataFrame(featureSet.get_values()[:, item])]
                selectedFeatureSet = pandas.concat(frames, axis=1)
                #   get the name of selected feature
                colName = list(featureSet.columns.values)[item]
                selectedFeatureSetNames = numpy.append(selectedFeatureSetNames, colName)
                #   add the column names to selectedFeatureSet dataframe
                selectedFeatureSet.columns = selectedFeatureSetNames
                #   delete selected feature from the deepcopy of the whole dataset
                del featureSet[colName]
            #   decrease number of features counter
            k -= 1

        #   from the second to the last feature to be selected I have to consider the set with the already
        #   selected features
        else:
            selectedFeature = forwardSelection(featureSet, classLabels, type, selectedFeatureSet)
            if selectedFeature is not None:
                frames = [selectedFeatureSet, pandas.DataFrame(featureSet.get_values()[:, selectedFeature])]
                selectedFeatureSet = pandas.concat(frames, axis=1)
                colName = list(featureSet.columns.values)[selectedFeature]
                selectedFeatureSetNames = numpy.append(selectedFeatureSetNames, colName)
                selectedFeatureSet.columns = selectedFeatureSetNames
                del featureSet[colName]
            k -= 1
        print "Selected feature " + str(kStart - k) + " " + type

    listRes = [featureSet, selectedFeatureSet]
    return listRes
