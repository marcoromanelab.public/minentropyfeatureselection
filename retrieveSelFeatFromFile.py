import sys
import numpy

def retrieveSelFeatFromFile(array):
    #print array
    shannonFeat = []
    renyiFeat = []
    index = [i for i, s in enumerate(array) if 'RENYI' in s]
    #print index[0]

    #Shannon
    for i in range(1, index[0]):
        #print array[i]
        if "f" in array[i]:
            if "\n" in array[i]:
                tmp = array[i][:-1]
            else:
                tmp = array[i]
            shannonFeat.append(tmp)

    #print "shannon", shannonFeat

    for i in range(index[0] + 1, len(array)):
        #print array[i]
        if "f" in array[i]:
            if "\n" in array[i]:
                tmp = array[i][:-1]
            else:
                tmp = array[i]
            renyiFeat.append(tmp)

    temp = shannonFeat[0].split(' ')
    temp = numpy.asarray(temp)
    a = []
    for el in temp:
        if el != "":
            a.append(el)
    shannonFeat = a

    temp = renyiFeat[0].split(' ')
    temp = numpy.asarray(temp)
    a = []
    for el in temp:
        if el != "":
            a.append(el)
    renyiFeat = a

    return [shannonFeat, renyiFeat]
